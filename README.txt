
This is the stock_chart module. It displays a chart for a given stock market
ticker symbol.

To use it, you open the Options fieldset and change anything you want changed.
If you want those changes saved for future charts, click on the Save chart
options button. If you just want to see the chart, click on the Show chart
button.
